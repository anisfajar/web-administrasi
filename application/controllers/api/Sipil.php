<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Sipil extends REST_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        header('Content-Type: application/json');
        $this->load->database();
        $this->load->model('m_sipil');
    }

    public function index_post()
    {
        $nik        = $this->post('nik');
        $warga      = $this->m_sipil->getbynik($nik)->row();
        $jmlwarga   = count($warga);

        if ($nik == "") {
            $result = [
                'status'    => FALSE,
                'warning'   => 'Maaf, NIK harus diisi terlebih dahulu'
            ];
            echo json_encode($result);
        } else {
            if(strlen($nik) > 16) {
                $result = [
                    'status'    => FALSE,
                    'warning'   => 'Maaf, NIK tidak valid'
                ];
                echo json_encode($result);
            } else {
                if ($jmlwarga == 0) {
                    $result = [
                        'status'    => FALSE,
                        'warning'   => 'Maaf, data tidak ada'
                    ];
                    echo json_encode($result);
                } else {
                    $result = [
                        'status'    => TRUE,
                        'data'      => $warga
                    ];
                    echo json_encode($result);
                }
            }
        }
    }
}